<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Hobby;
use Illuminate\Support\Facades\Auth;
use Validator;

class HobbyController extends Controller
{

    public $successStatus = 200;

    public function create_hobby(Request $request) {
        $validator = Validator::make($request->all(), [
          'hobby' => 'required',
        ]);

        $hobby = Hobby::create([
          'user_id' => Auth::id(),
          'hobby'   => $request->hobby,
        ]);

        if($hobby) {
          return response()->json(['success' => 'Hobby berhasil ditambahkan'], 200);
        }

        else {
          return response()->json(['error' =>'Kesalahan pada sistem'], 500);
        }
    }

    public function read_hobby() {
        $hobby = Hobby::where('user_id', Auth::id())->get();

        if($hobby) {
          return response()->json(['data' => $hobby], 200);
        }

        else {
          return response()->json(['error' =>'Kesalahan pada sistem'], 500);
        }
    }

    public function update_hobby(Request $request) {
        $validator = Validator::make($request->all(), [
          'id'      => 'required',
          'hobby'   => 'required',
        ]);

        $hobby = Hobby::where('id', $request->id)->where('user_id', Auth::id())->update(['hobby' => $request->hobby]);

        if($hobby) {
          return response()->json(['success' => 'Hobby berhasil diupdate'], 200);
        }

        else {
          return response()->json(['error' =>'Kesalahan pada sistem'], 500);
        }
    }

    public function delete_hobby(Request $request) {
        $validator = Validator::make($request->all(), [
          'id'      => 'required',
        ]);

        $hobby = Hobby::where('id', $request->id)->where('user_id', Auth::id())->delete();

        if($hobby) {
          return response()->json(['success' => 'Hobby berhasil dihapus'], 200);
        }

        else {
          return response()->json(['error' =>'Kesalahan pada sistem'], 500);
        }
    }

}
