<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class AdminController extends UserController
{
    public $successStatus = 200;
    
    protected function create_user(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'        => 'required',
            'email'       => 'required|email',
            'password'    => 'required',
            'c_password'  => 'required|same:password',
            'level'       => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        if(User::where('email', $request->email)->first()) {
          return response()->json(['error'=>'Email telah digunakan'], 400);
        }

        if(User::create($input)) {
          return response()->json(['success'=>'User berhasil dibuat'], $this->successStatus);
        }

        else {
          return response()->json(['error' =>'Kesalahan pada sistem'], 500);
        }
    }

    protected function read_user() {
        return response()->json(['data' => User::get()], 200);
    }

    protected function update_user(Request $request) {
        $validator = Validator::make($request->all(), [
            'id'          => 'required',
            'name'        => 'required',
            'level'       => 'required',
            'active'      => 'required',
        ]);

        $user = User::where('id', $request->id)->update([
          'name'    => $request->name,
          'level'   => $request->level,
          'active'  => $request->active,
        ]);

        if($user) {
          return response()->json(['success'=>'User berhasil diupdate'], $this->successStatus);
        }

        else {
          return response()->json(['error' =>'Kesalahan pada sistem'], 500);
        }
    }

    protected function delete_user(Request $request) {
        $validator = Validator::make($request->all(), [
            'id'          => 'required',
            'permanent'   => 'required',
        ]);

        if($request->permanent == 1) {
          if(User::where('id', $request->id)->delete()) {
            return response()->json(['success'=>'User berhasil dihapus permanen'], $this->successStatus);
          }

          else {
            return response()->json(['error' =>'Kesalahan pada sistem'], 500);
          }
        }

        else if($request->permanent == 0) {
          if(User::where('id', $request->id)->update(['active' => 0])) {
            return response()->json(['success'=>'User berhasil dihapus'], $this->successStatus);
          }

          else {
            return response()->json(['error' =>'Kesalahan pada sistem'], 500);
          }
        }
    }

}
