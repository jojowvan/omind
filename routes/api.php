<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\LoginController;
use App\Http\Controllers\API\AdminController;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\HobbyController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [LoginController::class, 'login']);
Route::post('register', [LoginController::class, 'register']);

Route::group(['middleware' => 'auth:api'], function() {

  Route::group(['prefix' => 'admin', 'middleware' => 'admin.access'], function() {
      Route::post('create-user', [AdminController::class, 'create_user']);
      Route::get('read-user', [AdminController::class, 'read_user']);
      Route::put('update-user', [AdminController::class, 'update_user']);
      Route::delete('delete-user', [AdminController::class, 'delete_user']);
  });

  Route::post('create-hobby', [HobbyController::class, 'create_hobby']);
  Route::get('read-hobby', [HobbyController::class, 'read_hobby']);
  Route::put('update-hobby', [HobbyController::class, 'update_hobby']);
  Route::delete('delete-hobby', [HobbyController::class, 'delete_hobby']);

  Route::get('who-am-i', [UserController::class, 'details']);
  Route::post('logout', [LoginController::class, 'logout']);
});
